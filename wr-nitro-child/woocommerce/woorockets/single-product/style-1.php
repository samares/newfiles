<?php
/**
 * @version    1.0
 * @package    WR_Theme
 * @author     WooRockets Team <support@woorockets.com>
 * @copyright  Copyright (C) 2014 WooRockets.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.woorockets.com
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $post;

$wr_nitro_options = WR_Nitro::get_options();

// Get sale price dates
$countdown = get_post_meta( get_the_ID(), '_show_countdown', true );
$start     = get_post_meta( get_the_ID(), '_sale_price_dates_from', true );
$end       = get_post_meta( get_the_ID(), '_sale_price_dates_to', true );
$now       = date( 'd-m-y' );

$is_bookings = ( class_exists( 'WC_Bookings' ) && $product->is_type( 'booking' ) );
$class_gravity_form = WR_Nitro_Helper::check_gravityforms( $post->ID ) ? ' custom-gravity-form' : NULL;

// Catalog mode
$wr_catalog_mode = $wr_nitro_options['wc_archive_catalog_mode'];

// Show custom button
$wr_show_button = $wr_nitro_options['wc_archive_catalog_mode_button'];

// Custom button action
$wr_show_button_action = $wr_nitro_options['wc_archive_catalog_mode_button_action'];

// Custom button action text
$wr_show_button_text = $wr_nitro_options['wc_archive_catalog_mode_button_action_simple'];

//Get custom content
$wr_custom_content_position = $wr_nitro_options['wc_single_product_custom_content_position'];
$wr_custom_content_data		= $wr_nitro_options['wc_single_product_custom_content_data'];

//Get custom message for sale product
$mes = get_post_meta( get_the_ID(), '_message_product_sale', true );
$recipientsName = get_post_meta($post->ID, '_recipients_name', true);
$setGoal = get_post_meta($post->ID, '_recipients_set_goal', true);
$occasion = get_post_meta($post->ID, '_recipients_occasion', true);
$setGoalId = false;

if($setGoal){

    preg_match('/\(Product ID: (.*?)\)/', $setGoal, $match);
    $setGoalId = (isset($match[1])) ? $match[1] : false;

}

?>

<div id="product-<?php the_ID(); ?>" <?php post_class( 'style-1' . ( is_customize_preview() ? ' customizable customize-section-product_single' : '' ) . $class_gravity_form ); ?>>

<section class="campaign-first-scr">
   <div class="container custom-campaign">
      <div class="row">
         <div class="col-12">
             <h1 class="campaign-title"><?php echo $occasion .' N-Cahoots for '. $recipientsName; ?></h1>
         </div>
      </div>
      <div class="row justify-content-center">
         <div class="col-md-5">
            <div class="row">
               <div class="col-12"> </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="campaign-desc-title">
                     <!-- <img src="http://n-cahoots.com/wp-content/uploads/2019/03/img1.jpg" /> -->
                     <?php
                     /**
                      * woocommerce_before_single_product_summary hook
                     *
                     * @hooked woocommerce_show_product_sale_flash - 10
                     * @hooked woocommerce_show_product_images - 20
                     */
                     do_action( 'woocommerce_before_single_product_summary' );
                  ?>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="campaign-desc-title custom-campaign-desc-title">Description:</div>
                  <div class="campaign-desc-cont">
                  <?php
                     $short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );

                     if ( !$short_description ) {
                        $short_description = '';
                     }

                  ?>               
                     <?php echo $short_description; // WPCS: XSS ok. ?>               
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-6 offset-md-1">
            <div class="custom_compaign_info_block">
               <div class="row">
                  <div class="col-6 text-right campaign-data-tit">
                     <p>GOAL:</p>
                  </div>
                  <div class="col-6 text-center campaign-data-val custom-campaign-data-val">
                     <?php the_title( '<p>', '</p>' ); ?>
                  </div>
               </div>
               <div class="row">
                  <div class="col-6 text-right campaign-data-tit">
                     <p>Days Left:</p>
                  </div>
                  <div class="col-6 text-center campaign-data-val campaign-data-val-red">
                     <p><?php echo WPNEOCF()->dateRemaining(); ?></p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-6 text-right campaign-data-tit">
                     <p>Money left to raise:</p>
                  </div>
                  <div class="col-6 text-center campaign-data-val">
                     <p>$<?php echo (wpneo_crowdfunding_get_total_goal_by_campaign(get_the_ID()) - wpneo_crowdfunding_get_total_fund_raised_by_campaign() );?></p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-6 text-right campaign-data-tit">
                     <p>Contributed People:</p>
                  </div>
                  <div class="col-6 text-center campaign-data-val">
                     <p>5</p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-12">
                     <div class="camp-progress-bar">
                        <div class="camp-bar">
                           <div class="camp-lbl-collected"><span class="camp-lbl-collected-val"><?php echo wpneo_crowdfunding_price(wpneo_crowdfunding_get_total_fund_raised_by_campaign()); ?></span>Collected</div>
                        </div>
                        <div class="camp-lbl-rest"><span class="camp-lbl-collected-rest">$0</span>&nbsp; is to go</div>
                        <div class="camp-lbl-collected"><span class="camp-lbl-collected-goal">$<?php echo (wpneo_crowdfunding_get_total_goal_by_campaign(get_the_ID()) - wpneo_crowdfunding_get_total_fund_raised_by_campaign() );?></span>Goal</div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="people-title"><span class="people-title-count">12</span> People Who Made a Contribution:</div>
                  <div class="people-count">Last 5 people</div>
                  <div class="people-photo-wrap">
                     <div class="people-photo-item">
                        <a href="#">
                           <img title="Man1" src="http://n-cahoots.com/wp-content/uploads/2019/03/tramp.jpg" alt="" />                           
                        </a>
                        <span class="people-photo-item-name">Last Name</span>
                        <span class="people-photo-item-name">First Name</span></a>
                     </div>
                     <div class="people-photo-item">
                        <a href="#">
                           <img title="Man1" src="http://n-cahoots.com/wp-content/uploads/2019/03/tramp.jpg" alt="" />
                        </a>
                        <span class="people-photo-item-name">Last Name</span>
                        <span class="people-photo-item-name">First Name</span>
                     </div>
                     <div class="people-photo-item">
                        <a href="#">
                           <img title="Man1" src="http://n-cahoots.com/wp-content/uploads/2019/03/tramp.jpg" alt="" />
                        </a>
                        <span class="people-photo-item-name">Last Name</span>
                        <span class="people-photo-item-name">First Name</span>
                     </div>
                     <div class="people-photo-item">
                        <a href="#">
                           <img title="Man1" src="http://n-cahoots.com/wp-content/uploads/2019/03/tramp.jpg" alt="" />
                        </a>
                        <span class="people-photo-item-name">Last Name</span>
                        <span class="people-photo-item-name">First Name</span>
                     </div>
                     <div class="people-photo-item">
                        <a href="#">
                           <img title="Man1" src="http://n-cahoots.com/wp-content/uploads/2019/03/tramp.jpg" alt="" />
                        </a>
                        <span class="people-photo-item-name">Last Name</span>
                        <span class="people-photo-item-name">First Name</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12"><button class="btn btn-primary btn-cotrib sbmt-btn sbmt-step-btn sbmt-step-btn-custom" type="button">Contribute</button></div>               
            </div>
         </div>
      </div>
   </div>
</section>
<?php /*
<button class="tablink" onclick="openPage('Home', this, 'red')">Home</button>
<button class="tablink" onclick="openPage('News', this, 'green')" id="defaultOpen">News</button>
<button class="tablink" onclick="openPage('Contact', this, 'blue')">Contact</button>
<button class="tablink" onclick="openPage('About', this, 'orange')">About</button>

<div id="Home" class="tabcontent">
  <h3>Home</h3>
  <p>Home is where the heart is..</p>
</div>

<div id="News" class="tabcontent">
  <h3>News</h3>
  <p>Some news this fine day!</p> 
</div>

<div id="Contact" class="tabcontent">
  <h3>Contact</h3>
  <p>Get in touch, or swing by for a cup of coffee.</p>
</div>

<div id="About" class="tabcontent">
  <h3>About</h3>
  <p>Who we are and what we do.</p>
</div>
*/ ?>
		<?php

// echo woocommerce_upsell_display(3); //SHOW RELATED PRODUCTS
      // if ( shortcode_exists( 'pt_view' ) ) :
         // $product_id   = $product->get_id();
			// $pids = array();
			// $query_obj = $products;
			// while ( $query_obj ? $query_obj->have_posts() : have_posts() ) :
			// 	$query_obj ? $query_obj->the_post() : the_post();
			// 	$pids[] = get_the_ID();
			// endwhile;
			//echo do_shortcode( '[pt_view id="VIEW_ID" post_id="8886"]' );
		// else :
			?>
			<?php //while ( $products->have_posts() ) : $products->the_post(); ?>

				<?php //wc_get_template_part( 'content', 'product' ); ?>

			<?php // endwhile; // end of the loop. ?>
		<?php //endif; ?>

<!-- new -->
    <section class="campaign-first-scr">
        <div class="container custom-campaign">
            <div class="row">
                <div class="col-12">
                    <h2 class="camp-h2">N-Cahoots is a Crowd Gifting Service.</h2>
                    <p class="camp-lead">All the participants are making this unique surprise gift for <?php echo $recipientsName; ?>. All you nedd is to do 3 easy steps: Contribute to the Gift for <?php echo $recipientsName; ?>, Answer a few questions about <?php echo $recipientsName; ?> in order to make the perfect gift, invite more friends to participate in N-cahoots. Just imagine how <?php echo $recipientsName; ?> will be pleased when find out that all of his/her friend think about him/her!</p>
                    <p class="camp-lead camp-lead-bold" style="color: #0171ed;">The more money You collect The Better Gifts <?php echo $recipientsName; ?> Will Recieve!</p>
                    <p class="camp-lead camp-lead-bold" style="color: #0171ed;">Remember: Your time is limited!</p>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-5" style="">
                    <h3 class="camp-h3" style="">Make it special!</h3>
                    <p class="spec-ideas" style="">Contribute your ideas about <?php echo $recipientsName; ?></p>

                    <form class="camp-form-about" style="">
                        <div class="form-group">
                            <label for="camp-qst-about" class="camp-qst-about-label">What <?php echo $recipientsName; ?> favoutite color?</label>
                            <textarea id="camp-qst-about" class="form-control" rows="7" style="height: 36px;"></textarea>
                        </div>
                        <button class="btn btn-primary sbmt-btn sbmt-step-btn sbmt-step-btn-custom-mis" type="submit">Submit</button>

                    </form>
                </div>
                <!-- ASSIGN PRODUCT CONTAINER -->
                <?php
                if($setGoalId){

                    $title = get_the_title($setGoalId);
                    $link = get_the_permalink($setGoalId);
                    $image = get_the_post_thumbnail($setGoalId);
                    $short_desc = get_the_excerpt($setGoalId); ?>
                    
                    <div class="col-lg-6 offset-lg-1">
                        <div class="row">
                            <div class="col-12">
                                <div class="camp-curr-goal">Current Goal: <?php echo $title ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="curr-gaol-img-wrap">
                                    <?php echo $image ?>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="curr-goal-desc-title">Gift Set Description</div>
                                        <div class="curr-goal-desc-val"><?php echo $short_desc ?></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row linked_product_feature">
                            <div class="col-sm-4">
                                <div class="curr-goal-feature"># Feature 1</div>
                            </div>
                            <div class="col-sm-4">
                                <div class="curr-goal-feature"># Feature 2</div>
                            </div>
                            <div class="col-sm-4">
                                <div class="curr-goal-feature"># Feature 3</div>
                            </div>
                        </div>
                    </div>
                    <script>
                        jQuery(document).ready(function($){
                            // Code goes here
                            $(".custom-campaign-data-val").html("<?php echo $title ?>");
                        });
                    </script>

                <?php } ?>
                <!-- END ASSIGN PRODUCT CONTAINER -->
            </div>
        </div>
    </section>
    <section class="campaign-first-scr custom-campaign-first-scr">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="camp-h2 camp-h2-big-mrg">3 Steps How to Make this N-Cahoots Special!</h2>
                </div>
            </div>
            <div class="row custom-camp-spec-steps">
                <div class="col-lg-4 camp-spec-steps">
                    <div class="camp-steps-tit">Contribute to the gift</div>
                    <div class="camp-steps-subtit">The more money you collect The Better Gifts <?php echo $recipientsName; ?> Will Recive!</div>
                    <form>
                        <div class="form-check form-check-steps">
                            <label class="form-check-label" for="radio1">$25</label>
                            <input id="radio1" class="form-check-input" name="radio" type="radio" value="$25" />
                        </div>
                        <div class="form-check form-check-steps">
                            <label class="form-check-label" for="radio2">$35</label>
                            <input id="radio2" class="form-check-input" name="radio" type="radio" value="$35" />
                        </div>
                        <div class="form-check form-check-steps">
                            <label class="form-check-label" for="radio3">$50</label>
                            <input id="radio3" class="form-check-input" name="radio" type="radio" value="$50" />
                        </div>
                        <div class="form-check form-check-steps">
                            <label class="form-check-label" for="radio4">$100</label>
                            <input id="radio4" class="form-check-input" name="radio" type="radio" value="$100" />
                        </div>
                        <div class="form-check form-check-steps">
                            <label class="form-check-label" for="radio5">$200</label>
                            <input id="radio5" class="form-check-input" name="radio" type="radio" value="$200" />
                        </div>
                        <div class="form-check form-check-other">
                            <input class="form-control other-sum" type="text" placeholder="Other" />
                            <input id="radio6" class="form-check-input" name="radio" type="radio" value="option1" />
                        </div>
                        <button class="btn btn-primary sbmt-btn sbmt-step-btn sbmt-step-btn-custom-3-steps" type="submit">Donate</button>

                    </form>
                </div>
                <div class="col-lg-4 camp-spec-steps">
                    <div class="camp-steps-tit">Take a Short Quiz</div>
                    <div class="camp-second-step-desc">Take a short quiz and make a personal N-Cahoots gift for <?php echo $recipientsName; ?></div>
                    <button class="btn btn-primary sbmt-btn sbmt-step-btn sbmt-step-btn-custom-3-steps" type="submit">Take a Quiz</button>

                </div>
                <div class="col-lg-4 camp-spec-steps">
                    <div class="camp-steps-tit">Invite More People to Participate</div>
                    <div class="camp-third-step-btns"><a class="btn btn-outline-primary" role="button" href="#">Email</a>
                        <a class="btn btn-outline-primary" role="button" href="#">Facebook</a>
                        <a class="btn btn-outline-primary" role="button" href="#">Twitter</a></div>
                    <div class="camp-third-step-link">
                        <div class="camp-steps-tit">Direct Link:</div>
                        <div class="camp-third-step-link-wrap">
                            <input class="form-control" type="text" value="http://n-cahoots.com/somelink" style="border-color: #0680f2 !important;" />
                        </div>
                        <button class="btn btn-primary sbmt-btn sbmt-step-btn sbmt-step-btn-custom-3-steps" type="submit">Copy Link</button>

                    </div>
                </div>
            </div>
    </section>
    <section class="campaign-first-scr custom-campaign-first-scr custom-campaign-first-scr-share-idea">
        <div class="container container-last">
            <div class="row">
                <div class="col-12" style="display:none">
                    <h2 class="camp-h2 camp-h2-big-mrg">How well do you know <?php echo $recipientsName; ?>?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="camp-gist custom-camp-gist-1">Do you know what could be the perfect gist for <?php echo $recipientsName; ?>?
                        <br/>
                        <br/><b>Share your idea with us!</b></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <form class="camp-gist-form">
                        <input class="form-control" name="what" type="text" placeholder="What is it?" />
                        <input class="form-control" name="link" type="text" placeholder="Insert Link here" />
                        <div class="sbmt-step-btn-custom-share-idea-devider"></div>
                        <button class="btn btn-primary sbmt-btn sbmt-step-btn sbmt-step-btn-custom-share-idea" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
<!-- new -->
<!-- ****************************** -->


	<div class="p-single-top oh pr">
		<div class="p-single-images">
			<?php
				/**
				 * woocommerce_before_single_product_summary hook
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				//do_action( 'woocommerce_before_single_product_summary' );
			?>
		</div>
		<div class="p-single-info">
			<?php
				// if ( $wr_nitro_options['wc_single_breadcrumb'] ) {
				// 	echo '<div class="mgb10">';
				// 		woocommerce_breadcrumb();
				// 	echo '</div>';
				// }
			?>
			<?php
				/**
				 * woocommerce_single_product_summary hook.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 */
				//do_action( 'woocommerce_single_product_summary' );
			?>
		</div>
		<?php if ( 'yes' == $countdown && $end && date( 'd-m-y', $start ) <= $now ) : ?>
			<div class="product__countdown pa bgw">
				<div class="wr-nitro-countdown fc jcsb tc aic" data-time='{"day": "<?php echo date( 'd', $end ); ?>", "month": "<?php echo date( 'm', $end ); ?>", "year": "<?php echo date( 'Y', $end ); ?>"}'></div>
			</div>
		<?php endif; ?>
	</div>

<?php /* 
	<div class="p-single-middle clear">
		<div class="fl mgt10">
			 <?php
				 if ( class_exists( 'WR_Share_For_Discounts' ) ) {
				 	$product_id   = $product->get_id();
					$sfd          = get_option( 'wr_share_for_discounts' );
					$settings     = $sfd['enable_product_discount'];
					$product_data = WR_Share_For_Discounts::get_meta_data( $product_id );

					if ( $settings != 1 || $product_data['enable'] != 1 ) {
						echo WR_Nitro_Pluggable_WooCommerce::woocommerce_share();
					}
				} else {
					echo WR_Nitro_Pluggable_WooCommerce::woocommerce_share();
				}
 			?>
		</div>

		<?php
			if ( ! $is_bookings ) :
				echo '<div class="p-single-action fr clearfix">';
					woocommerce_template_single_add_to_cart();
				echo '</div>';
			endif;

			if ( $wr_catalog_mode && $wr_show_button ) {
				echo '<div class="p-single-action fr clearfix">';
					if ( $wr_show_button_action == 'simple' ) {
						echo '<a target="_blank" rel="noopener noreferrer" class="button wr-btn wr-btn-solid" href="' . esc_attr( $wr_show_button_text ) . '">' . esc_attr( $wr_nitro_options['wc_archive_catalog_mode_button_text'] ) . '</a>';
					} else {
						echo '<a class="button wr-btn wr-btn-solid wr-open-cf7" href="#wr-cf7-form">' . esc_attr( $wr_nitro_options['wc_archive_catalog_mode_button_text'] ) . '</a>';
						echo '<div id="wr-cf7-form" class="mfp-hide">';
							echo do_shortcode( '[contact-form-7 id="' . esc_attr( $wr_nitro_options['wc_archive_catalog_mode_button_action_cf7'] ) . '"]' );
						echo '</div>';
					}
				echo '</div>';
			}
		?>

		<div class="p-meta tu fr mgt10 mgr10">
			<span class="availability mgl10">
				<?php $availability = $product->get_availability(); ?>
				<span class="meta-left"><?php esc_html_e( 'Availability:', 'wr-nitro' ); ?></span>
				<span class="stock <?php echo ( $product->is_in_stock() ? 'in-stock' : 'out-stock' ); ?>">
					<?php
						if ( version_compare( WC_VERSION, '3.0.0', '<' ) ) {
							if ( $product->manage_stock == 'yes' && ! empty( $availability['availability'] ) ) :
								echo esc_html( $availability['availability'] );
							elseif ( $product->manage_stock == 'no' && $product->is_in_stock() ) :
								esc_html_e( 'In Stock', 'wr-nitro' );
							else :
								esc_html_e( 'Out Of Stock', 'wr-nitro' );
							endif;
						} else {
							if ( $product->get_manage_stock() && ! empty( $availability['availability'] ) ) :
								echo esc_html( $availability['availability'] );
							elseif ( ! $product->get_manage_stock() && $product->is_in_stock() ) :
								esc_html_e( 'In Stock', 'wr-nitro' );
							else :
								esc_html_e( 'Out Of Stock', 'wr-nitro' );
							endif;
						}
					?>
				</span>
			</span>
		</div>
	</div>
*/ ?>
	<div class="p-single-bot">
		<?php
			/**
			 * woocommerce_after_single_product_summary hook.
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			// do_action( 'woocommerce_after_single_product_summary' );
		?>

		<?php //wc_get_template( 'woorockets/single-product/builder.php' ); ?>
	</div>

	<?php //wc_get_template( 'woorockets/single-product/floating-button.php' ); ?>


</div><!-- #product-<?php the_ID(); ?> -->