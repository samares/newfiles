jQuery(document).ready(function($) {
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if(animating) return false;

	//validate first page
    if(!validPage()) return false;


	animating = true;





	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
        'transform': 'scale('+scale+')'
        // 'position': 'absolute'
      });
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".date").flatpickr({
	minDate: "today",
	dateFormat: "d.m.Y",
});
$(".time").flatpickr(
	{
    enableTime: true,
    noCalendar: true,
    dateFormat: "H:i",
    minDate: "9:00",
    maxDate: "19:00",
	
});

/*$('.time').timepicker();*/

	/*validation last page*/

    $(".submit").click(function(){
        valid = 1;	
        if(!$('input[name="pref-date"]').val()){
            valid = 0;
            displayError($('input[name="pref-date"]'));
        }
        if(!$('input[name="addr1"]').val()){
            valid = 0;
            displayError($('input[name="addr1"]'));
        }
        if(!$('input[name="addr2"]').val()){
            valid = 0;
            displayError($('input[name="addr2"]'));
        }
        if(!$('input[name="addr-city"]').val()){
            valid = 0;
            displayError($('input[name="addr-city"]'));
        }
        if(!$('input[name="addr-zip"]').val()){
            valid = 0;
            displayError($('input[name="addr-zip"]'));
        }
        if(!$('input[name="phone"]').val()){
            valid = 0;
            displayError($('input[name="phone"]'));
        }

		if(!valid) return false;
    	/*if(
            !$('input[name="rcpts_name"]').val() ||
            !$('input[name="rcpnt-age"]').val() ||
            !$('input[name="pref-date"]').val() ||
            !$('input[name="addr1"]').val() ||
            !$('input[name="addr2"]').val() ||
            !$('input[name="addr-city"]').val() ||
            !$('input[name="addr-zip"]').val() ||
            !$('input[name="phone"]').val()
		) {
            alert('All fields are required.');
            return false;
		}*/
    });

    $("#phone").mask("(999) 999-9999");

    $('input[name="rcpts_name"]').mask('ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ',
		{translation:  {'Z': {pattern: /[A-Za-z\s\-]/}}});

    var options =  {
        onChange: function(cep){
        	if(cep > 150) {
        		$el = $('input[name="rcpnt-age"]');
                $el.val(150);
                displayError($el);

			}
        }
    };



    $('input[name="rcpnt-age"]').mask('999', options);

    $('input[name="addr-zip"]').mask('Z9999', {translation:  {'Z': {pattern: /[1-9]/}}});







});

function validPage(){
    var valid = 1;
    if(!$('input[name="rcpts_name"]').val()){
        valid = 0;
        displayError($('input[name="rcpts_name"]'));
    }
    if(!$('input[name="rcpnt-age"]').val()){
        valid = 0;
        displayError($('input[name="rcpnt-age"]'));
    }

    return valid;
}

function displayError($el){
    $el.next().show();
    $el.addClass("field-error");


    setTimeout(function(){
        $el.next().hide();
        $el.removeClass("field-error");
    }, 2000);
}

/*document.addEventListener("DOMContentLoaded", function(event)
	{
    timepicker.load({
	      interval: 30
	    });
	});*/

$('[name="set-goal"]').on('click', function() {
	$('[data-type="set-goal"]').each(function() {
		$(this).removeClass('active');
	})
	var id = $(this).attr('id');
	$('[data-id="'+id+'"]').addClass('active');
});

