<?php
/**
 * @version    1.0
 * @package    Nitro
 * @author     WooRockets Team <support@woorockets.com>
 * @copyright  Copyright (C) 2014 WooRockets.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.woorockets.com
 */

/**
 * Enqueue script for child theme
 */
function wr_nitro_child_enqueue_scripts() {
	wp_enqueue_script( 'nitro-child-custom-script', get_stylesheet_directory_uri() . '/js/main.js', array(), '1.0.0', true);	
	
	wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
	wp_enqueue_style( 'nitro-child-custom-style', get_stylesheet_directory_uri() . '/css/main.css' );
	wp_enqueue_style( 'wr-nitro-child-style', get_stylesheet_directory_uri() . '/style.css' );

    wp_register_script( 'jquery-easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', array(), '1.0.0', true );
    wp_register_script( 'main-form-js', get_stylesheet_directory_uri() . '/js/main-form.js', array('jquery'), '1.0.0', true );
    wp_register_script( 'flatpickr-js', get_stylesheet_directory_uri() . '/js/flatpickr.min.js', array('jquery'), '1.0.0', true );
    wp_register_script( 'phone-mask-js', get_stylesheet_directory_uri() . '/js/mask.min.js', array('jquery', 'main-form-js'), '1.0.0', true );    
    
    wp_register_script( 'font-awesome2', 'https://use.fontawesome.com/releases/v5.1.1/js/all.js', array(), '1.0.0', true );
    wp_enqueue_script( 'font-awesome2' );

    wp_register_script( 'font-awesome2', 'https://use.fontawesome.com/releases/v5.1.1/js/all.js', array(), '1.0.0', true );
    wp_enqueue_script( 'font-awesome2' );

    wp_register_style( 'flatpickr-css', get_stylesheet_directory_uri() . '/css/flatpickr.min.css', array(), '1.0.0' );
    wp_register_style( 'main-form-css', get_stylesheet_directory_uri() . '/css/main-form.css', array(), '1.0.0' );

    

}
add_action( 'wp_enqueue_scripts', 'wr_nitro_child_enqueue_scripts', 1000000000 );

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/*carbon-fiedls include*/
require_once get_stylesheet_directory() .'/inc/carbon-fields/carbon-fields-plugin.php';
/*Theme options*/
add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
	require_once( 'inc/custom-fields/theme-options.php' );
	require_once( 'inc/custom-fields/page-options.php' );
	\Carbon_Fields\Carbon_Fields::boot();
}
/*end carbon-fields*/

function post_title_shortcode(){
    return get_the_title();
}
add_shortcode('post_title','post_title_shortcode');

add_shortcode('product_data','custom_product_function');
function custom_product_function($atts)
{
    $post_id = $atts['id'];
    $title = get_the_title($post_id);
    $link = get_the_permalink($post_id);
    $image = get_the_post_thumbnail($post_id);
    // $short_desc = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
    // $short_desc = $product->get_short_description();
    $short_desc = get_the_excerpt($post_id);
    	
	$data = '
	<div class="col-lg-6 offset-lg-1">
		<div class="row">
			<div class="col-12">
				<div class="camp-curr-goal">Current Goal: '.$title.'</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div class="curr-gaol-img-wrap">
				'.$image.'
				</div>
			</div>
			<div class="col-md-7">
				<div class="row">
					<div class="col-12">
					<div class="curr-goal-desc-title">Gift Set Description</div>
					<div class="curr-goal-desc-val">
						'.$short_desc.'
						
					</div>
					</div>
				</div>
				
			</div>
		</div>
		<div class="row linked_product_feature">
			<div class="col-sm-4">
			<div class="curr-goal-feature"># Feature 1</div>
			</div>
			<div class="col-sm-4">
			<div class="curr-goal-feature"># Feature 2</div>
			</div>
			<div class="col-sm-4">
			<div class="curr-goal-feature"># Feature 3</div>
			</div>
		</div>
		</div>
		<script>		
			jQuery(document).ready(function($){		
			// Code goes here
				$(".custom-campaign-data-val").html("'.$title.'");
			});
		</script>
	';

    return $data;
}


//Set form
function set_form_process() {

    $meta = array();

    $meta['_recipients_gift'] = (!$_POST['gift-recipient']) ? 'For Myself' : 'For someone else';
    $title = ($_POST['rcpts_name']) ? sanitize_text_field($_POST['rcpts_name']) : 'Not set';
    $meta['_recipients_name'] = $title;
    $meta['_recipient_age'] = sanitize_text_field($_POST['rcpnt-age']);
    $meta['_recipient_gender'] = sanitize_text_field($_POST['recipient-gender']);
    $meta['_recipients_relationship'] = sanitize_text_field($_POST['recipient-relationship']);
    $meta['_recipients_occasion'] = sanitize_text_field($_POST['gift-occasion']);
    //price and product set
    $d = explode('|',$_POST['set-goal']);
    //price
    $meta['_nf_funding_goal'] = $d[1];
    //choosen set
    $meta['_recipients_set_goal'] = get_the_title($d[0]) . ' (Product ID: ' .$d[0]. ')';
    $meta['_nf_duration_start'] = date('d.m.Y H:i');
    $meta['_nf_duration_end'] = $_POST['pref-date'] .' '. $_POST['pref-time'];
    $meta['_picked_date'] = sanitize_text_field($_POST['pref-date']);
    $meta['_recipients_picked_time'] = sanitize_text_field($_POST['pref-time']);
    $meta['_recipients_delivery'] = (!$_POST['gift-delivery']) ? 'To me' : 'To recipient';
    $meta['_recipients_address1'] = sanitize_text_field($_POST['addr1']);
    $meta['_recipients_address2'] = sanitize_text_field($_POST['addr2']);
    $meta['_recipients_city'] = sanitize_text_field($_POST['addr-city']);
    $meta['_recipients_state'] = sanitize_text_field($_POST['addr-state']);
    $meta['_recipients_zip'] = sanitize_text_field($_POST['addr-zip']);
    $meta['_recipients_phone'] = sanitize_text_field($_POST['phone']);

    $curUserId = (get_current_user_id()) ? get_current_user_id() : 1;

    $postDate= array(
        'post_title'    => $title,
        'post_status'   => 'publish',
        'post_type'   => 'product',
        'post_author'   => $curUserId
    );
    
    $id = wp_insert_post( $postDate, true);

    if( !is_wp_error($id) ){

        //set product type
        wp_set_object_terms( $id, 'crowdfunding', 'product_type' );

        //up date post meta if count > 0
        if(count($meta) > 0){

            foreach ($meta as $key => $val) {

                update_post_meta( $id, $key, $val );

            }
        }
    }

    wp_redirect( $_SERVER["HTTP_REFERER"] . '?mess=true' );
    exit;

}
add_action( 'admin_post_nopriv_set_form', 'set_form_process' );
add_action( 'admin_post_set_form', 'set_form_process' );
//Set form END


// Shortcode for Campaign Submit Form
add_shortcode( 'wpneo_crowdfunding_new_form','wpneo_shortcode_croudfunding_new_form' ); 

function wpneo_shortcode_croudfunding_new_form( $atts ){

    global $post, $wpdb;
    $html = '';
    $state = '<option value="Alabama">AL</option>
              <option value="Alaska">AK</option>
              <option value="Arizona">AZ</option>
              <option value="Arkansas">AR</option>
              <option value="California">CA</option>
              <option value="Colorado">CO</option>
              <option value="Connecticut">CT</option>
              <option value="Delaware">DE</option>
              <option value="Florida">FL</option>
              <option value="Georgia">GA</option>
              <option value="Hawaii">HI</option>
              <option value="Idaho">ID</option>
              <option value="Illinois">IL</option>
              <option value="Indiana">IN</option>
              <option value="Iowa">IA</option>
              <option value="Kansas">KS</option>
              <option value="Kentucky">KY</option>
              <option value="Louisiana">LA</option>
              <option value="Maine">ME</option>
              <option value="Maryland">MD</option>
              <option value="Massachusetts">MA</option>
              <option value="Michigan">MI</option>
              <option value="Minnesota">MN</option>
              <option value="Mississippi">MS</option>
              <option value="Missouri">MO</option>
              <option value="Montana">MT</option>
              <option value="Nebraska">NE</option>
              <option value="Nevada">NV</option>
              <option value="New Hampshire">NH</option>
              <option value="New Jersey">NJ</option>
              <option value="New Mexico">NM</option>
              <option value="New York">NY</option>
              <option value="North Carolina">NC</option>
              <option value="North Dakota">ND</option>
              <option value="Ohio">OH</option>
              <option value="Oklahoma">OK</option>
              <option value="Oregon">OR</option>
              <option value="Pennsylvania">PA</option>
              <option value="Rhode Island">RI</option>
              <option value="South Carolina">SC</option>
              <option value="South Dakota">SD</option>
              <option value="Tennessee">TN</option>
              <option value="Texas">TX</option>
              <option value="Utah">UT</option>
              <option value="Vermont">VT</option>
              <option value="Virginia">VA</option>
              <option value="Washington">WA</option>
              <option value="West Virginia">WV</option>
              <option value="Wisconsin">WI</option>
              <option value="Wyoming">WY</option>';

    //include css and js
    wp_enqueue_style( 'flatpickr-css' );
    wp_enqueue_style( 'main-form-css' );

    wp_enqueue_script('jquery-easing');
    wp_enqueue_script('main-form-js');
    wp_enqueue_script('flatpickr-js');
    wp_enqueue_script('phone-mask-js');

    if($_GET['mess'] == "true") $html .= '<p>Done!</p>';

    $html .= '<form action="' .esc_url( admin_url('admin-post.php') ). '" id="msform" method="post" >
  <!-- progressbar -->
  <ul id="progressbar">
    <li class="active">Step 1: Create an N-Cahoots Gift</li>
    <li>Step 2: Customize it</li>
    <li>Step 3: Delivery</li>
  </ul>
  <!-- fieldsets -->
  <fieldset>
    <h3 class="fs-subtitle">Please tell us for whom you plan this box?</h3>
    <!--<input type="radio" name="recipient">For Myself</input>
  <input type="radio" name="recipient">For Someone else</input>-->
  
  <!-- Switch -->

  <div class="input-set">
  	<div class="switcher-desc">For Whom you are planning the gift?</div>
	  	<div class="switcher">
			<div>For Myself</div><div class="onoffswitch">
				<input type="checkbox" name="gift-recipient" class="onoffswitch-checkbox" onclick="history();" id="myonoffswitch">
<label class=" onoffswitch-label" for="myonoffswitch"></label>
			</div><div>For someone else</div>
		</div>
	</div>
    
<div>

<div class="input-set">
  <label for="rcpnt__name">Recipients` name</label>
  <input type="text" name="rcpts_name" id="rcpnt__name" placeholder="Max" required />
  <span class="field-error">Please, input correct Name</span>
  </div>
  
</div>

<div class="input-set">
	<label for="rcpnt-age">Recipient Age</label>
	<input type="text" id="rcpnt-age" name="rcpnt-age" required placeholder="29"/>
	<span class="field-error">Please, input correct Age</span>
</div>

<div class="input-set">
	<label for="recipient_gender">Recipient Gender</label>
	<select name="recipient-gender" id="recipient_gender" required>
		<option value="female">female</option>
  		<option value="male">male</option>
  	</select>
</div>  
 
<div class="boxesrow_main">
<div class="input-set">
	<label for="recipient_gender">Your relationship</label>
	 <select name="recipient-relationship" id="recipient_relationship" required>
	  <option value="female">friend</option>
	  <option value="male">friend 1</option>
	</select>
</div>
</div> 

<div class="input-set">
	<label for="gift_occasion">Occasion</label>
	<select name="gift-occasion" id="gift_occasion" required>
	  <option value="Birthday">Birthday</option>
	  <option value="Birthday2">Birthday 2</option>
	</select>
</div>
	<input type="button" name="back" class="previous action-button inactive" value="Back" />
  	<input type="button" name="next" class="next action-button" value="Next"  />
  </fieldset>';

    // Получаем свойства
    /*extract(shortcode_atts(array(
        "tags" => ''
    ), $atts));
    ob_start();*/


    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 5,
        'order' => 'asc',
        'product_tag' => 'adult'
    );
    $product_header = '';
    $product_more = '';

    $loop = new WP_Query( $args );

    $product_count = $loop->post_count;


    $html .= '<fieldset>';
    $html .= '<h3 class="fs-subtitle">Set your Goal</h3>';
    $html .= '<div class="radio radio-goal">';

    if( $product_count > 0 ) :

        $i = 1;

        while ( $loop->have_posts() ) : $loop->the_post();
            global $product;
            global $post;

            $act = ($i == 1) ? ' checked' : '';
            $product_header .= '<div>';
            //$product_header .= '<label for="goal' .$i. '">' .$post->post_title. '</label>';
            $product_header .= '<label for="goal' .$i. '">' .get_the_title(). '</label>';
            $product_header .= '<input type="radio" name="set-goal" value="' .$post->ID. '|' .$product->get_price(). '" id="goal' .$i. '" ' .$act. '>$' .$product->get_price(). '</input>';
            $product_header .= '</div>';


            $act = ($i == 1) ? ' class="active"' : '';

            $product_more .= '<section data-id="goal' .$i. '" " ' .$act. ' data-type="set-goal">';
            $product_more .= '<div class="img-wrap"><img src="img/img1.jpg"></div>';
            $product_more .= '<div class="info-wrap">';
            $product_more .= '<h1 class="prod-title">' .get_the_title(). '</h1>';
            $product_more .= '<div class="prod-desc">' .get_the_content(). '</div>';
            $product_more .= '<div class="prod-features">';
            $product_more .= '<div class="feature-item"># Feature 1</div>';
            $product_more .= '<div class="feature-item"># Feature 2</div>';
            $product_more .= '<div class="feature-item"># Feature 3</div>';
            $product_more .= '</div>';
            $product_more .= '</div>';
            $product_more .= '</section>';

            $i++;



            /*echo "<p>" . $thePostID = $post->post_title. " </p>";
            if (has_post_thumbnail( $loop->post->ID ))
                echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog');
            else
                echo '<img src="'.$woocommerce->plugin_url().'/assets/images/placeholder.png" alt="" width="'.$woocommerce->get_image_size('shop_catalog_image_width').'px" height="'.$woocommerce->get_image_size('shop_catalog_image_height').'px" />';
        */
            endwhile;
    else :
        $product_header .= 'Any gifts';
    endif;

    $html .= $product_header;
    $html .= '</div>';
    $html .= '<div class="company-card">';
    $html .= $product_more;
    $html .= '</div>';
    $html .= '<input type="button" name="back" class="previous action-button" value="Back" />';
    $html .= '<input type="button" name="next" class="next action-button" value="Next" />';
    $html .= '</fieldset>';

    $html .= '
  

  <fieldset>
	<div class="input-set">
		<label for="pref-date">Preferred Date</label>
		<input type="text" name="pref-date" id="pref-date" placeholder="Pick the Date" class="date" required>
		<span class="field-error">Please, input correct Date</span>
	</div>

	<div class="input-set">
		<label for="pref-time">Preferred Time</label>
		<input type="text" name="pref-time" id="pref-time" class="time" placeholder="Pick the Time">
	</div>

	<div class="input-set">
		<div class="switcher-desc">Do you want us to deliver it to you or send it directly to the recipient?</div>
		<div class="switcher">
			<div>To me</div><div class="onoffswitch">
				<input type="checkbox" name="gift-delivery" class="onoffswitch-checkbox" id="giftdelivery">
				<label class="onoffswitch-label" for="giftdelivery"></label>
			</div><div>To recipient</div>
		</div>
	</div>

	<div class="input-set">
		<label for="addr1">Address</label>
		<input type="text" name="addr1" id="addr1" placeholder="Address Line 1" required />
		<span class="field-error">Please, input correct Address</span>
	</div>
	<div class="input-set has-group">
		<label for="addr2"></label>
		<div class="input-group address_gp">			
			<input type="text" name="addr2" id="addr2" placeholder="Address Line 2" required />
			<span class="field-error">Please, input correct Address Line 2</span>
			
			<input class="threein1" type="text" name="addr-city" id="addr-city" placeholder="City" required />
			<span class="field-error">Please, input correct City</span>
			
			<select name="addr-state" class="small-inp" required>' .$state. '</select>
			<input type="text" class="small-inp" name="addr-zip" id="addr-zip" placeholder="Zip" required />
			<span class="field-error">Please, input correct Zip</span>
			
			<input type="text" name="phone" id="phone"  required placeholder="Contact Phone number"/>
			<span class="field-error">Please, input correct Phone number</span>
		</div>
    	
    </div>

    <div class="input-set">
		<label for="addr2"></label>
    	
    </div>

    <div class="input-set">
		<label for="addr-city"></label>
    	
    </div>

    <div class="input-set">
		<label for="addr-city"></label>		
		<div class="label-wrap">			
			
		</div>
    </div>
    <div class="input-set">
		<label for="phone"></label>
    	
    </div>    
    
    <input type="button" name="back" class="previous action-button" value="Back" />
    <input type="hidden" name="action" value="set_form">
    <input type="submit" name="submit" class="submit action-button" value="Submit" />
  </fieldset>
</form>
';
    return $html;

}

